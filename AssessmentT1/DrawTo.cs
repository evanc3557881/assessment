﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace AssessmentT1
{
    class DrawTo:command
    {
        TextBox error;
        Graphic gr;
        PictureBox pictureBox;
        Graphics graphics;
        public DrawTo(TextBox error, Graphic gr, PictureBox pictureBox, Graphics graphics)
        {
            //giving the drawto class a reference to all the objects so it can access what it needs
            this.error = error;
            this.gr = gr;
            this.pictureBox = pictureBox;
            this.graphics = graphics;
        }
        public void execute(string[] words)
        {
            int? newX = null;//new point for pointer to move to
            int? newY = null;
            try
            {
                //check if the users input is a variable
                if (gr.checkVar(words[1]))
                {
                    newX = gr.variables[words[1]];
                }
                else
                //if not parse their input to an in and set it to the new coordinate
                {
                    newX = Int32.Parse(words[1]);
                }
            }
            catch
            {
                error.AppendText(words[1] + " is an incorrect parameter for " + words[0] + "\r\n");
            }
            try
            {
                if (gr.checkVar(words[2]))
                {
                    newY = gr.variables[words[2]];
                }
                else
                {

                    newY = Int32.Parse(words[2]);
                }
            }
            catch
            {
                error.AppendText(words[2] + " is an incorrect parameter for " + words[0] + "\r\n");
            }
            if (newX != null && newY != null)
            {
                graphics.DrawLine(gr.myPen, gr.xPos, gr.yPos, (int)newX, (int)newY);

                pictureBox.Refresh();//refreshes display to show changes

                gr.xPos = (int)newX;
                gr.yPos = (int)newY;
            }
        }
    }
}
