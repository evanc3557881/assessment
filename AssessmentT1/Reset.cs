﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AssessmentT1
{
    class Reset:command
    {
        TextBox error;
        Graphic gr;
        PictureBox pictureBox;
        Graphics graphics;
        public Reset(TextBox error, Graphic gr, PictureBox pictureBox, Graphics graphics)
        {
            this.error = error;
            this.gr = gr;
            this.pictureBox = pictureBox;
            this.graphics = graphics;
        }
        public void execute(string[] words)
        {
            gr.flashing.ResetThreads();
            gr.variables.Clear();
            graphics.Clear(Color.Transparent);
            gr.xPos = 0;
            gr.yPos = 0;
            gr.myPen = new Pen(Color.Black, 1);
            pictureBox.Refresh();

        }
    }
}
