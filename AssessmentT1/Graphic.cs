﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace AssessmentT1
{
    public class Graphic
    {
        public Graphics gr; //setting graphics context
        public Pen myPen;//inistializes new pen
        public int xPos, yPos;//coordinates of pointer
        public FlashingColor flashing;
        /// <summary>
        /// initializes the pointer to the top left at 0,0 and defaults the pen to black
        /// </summary>
        /// <param name="gr">Given place for the pen to draw on</param>
        public Graphic(TextBox error, Graphics gr)
        {
            this.gr = gr;
            xPos = yPos = 0;
            myPen = new Pen(Color.Black, 1);
            flashing = new FlashingColor(error);
        }
  
        public bool fillon;

        public bool checkVar(string variable)
        {
               return variables.ContainsKey(variable);
        }

        public Dictionary<string, int> variables = new Dictionary<string, int>();
        /// <summary>
        /// Variable function
        /// </summary>
        /// <param name="words"></param>
        /// <param name="error"></param>
        /// <example>
        /// An example of how the variable command works
        /// var num 50 sets a variable with the value 50 to num
        /// var num1 60 sets a variable with the value 60 to num1
        /// var num2 num1 + num Would set num 2 to the value of num + num1 (110)
        /// var num num - 30 would minus 30 from the num value and set it to num
        /// </example>
        public void Var(string[] words, TextBox error)
        {
            int var=0;
            try
            {
                var = Int32.Parse(words[2]);
            }
            catch
            
            { 
                if (variables.ContainsKey(words[2]))
                {
                    var = variables[words[2]];
                }
                else
                {
                    error.AppendText(words[2] + " is not and integer or varaible \r\n");
                    return;
                }

            }
            if (words.Length == 5)
            {
                int var2 = 0;
                try
                {
                    var2 = Int32.Parse(words[4]);
                }
                catch

                {
                    if (variables.ContainsKey(words[4]))
                    {
                        var2 = variables[words[4]];
                    }
                    else
                    {
                        error.AppendText(words[4] + " is not and integer or varaible \r\n");
                        return;
                    }

                }

                if (words[3].Equals("*"))
                {
                    var *= var2;
                }
                if (words[3].Equals("/"))
                {
                    var /= var2;
                }
                else if (words[3].Equals("+"))
                {
                    var += var2;
                }
                else if (words[3].Equals("-"))
                {
                    var -= var2;
                }
            }
            variables[words[1]] = var;




        }

    }
}

