﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AssessmentT1
{
    class Fill:command
    {
        TextBox error;
        Graphic gr;
        PictureBox pictureBox;
        Graphics graphics;
        public Fill(TextBox error, Graphic gr, PictureBox pictureBox, Graphics graphics)
        {
            this.error = error;
            this.gr = gr;
            this.pictureBox = pictureBox;
            this.graphics = graphics;
        }

        public void execute(string[] words)
        {
            gr.fillon = words[1] == "on";
        }
    }
}
