﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace AssessmentT1
{
    public class FlashingColor
    {
        TextBox error;

        List<Thread> Threads = new List<Thread>();
        public FlashingColor(TextBox error)
        {
            this.error = error;
        }

        public void execute(string[] words, command commands, Color color1, Color color2)
        {
                Threads.Add(new Thread(() => Flashing(color1, color2, words, commands)));
                Threads.Last<Thread>().Start();
        }
        public void Flashing(Color color1, Color color2, string[] words, command commands)
        {
            Pen myPen = new Pen(color1);
            Boolean flag = false;
            while(true)
            {
                if (flag)
                {
                   myPen = new Pen(color1);
                }
                else
                {
                   myPen = new Pen(color2);
                }
                commands.execute(words);
                flag = !flag;
                Thread.Sleep(500);
            }
        }
        public void ResetThreads()
        {
            for(int i =0; i < Threads.Count;i++)
            {
                Threads[i].Abort();
            }
            Threads.Clear();
        }
    }
}
