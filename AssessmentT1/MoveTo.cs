﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AssessmentT1
{
    class MoveTo:command
    {
        TextBox error;
        Graphic gr;
        PictureBox pictureBox;
        Graphics graphics;
        public MoveTo(TextBox error, Graphic gr, PictureBox pictureBox, Graphics graphics)
        {
            this.error = error;
            this.gr = gr;
            this.pictureBox = pictureBox;
            this.graphics = graphics;
        }
        public void execute(string[] words)
        {
            int? newX = null;
            int? newY = null;
            try
            {
                if (gr.checkVar(words[1]))
                {
                    newX = gr.variables[words[1]];
                }
                else
                {
                    newX = Int32.Parse(words[1]);//coverts parameter from string to integer
                }
            }


            
            catch
            {
                error.AppendText(words[1] + " is an incorrect parameter for " + words[0] + "\r\n");//if failed its an incorrect parameter
            }
            try
            {
                if (gr.checkVar(words[2]))
                {
                    newX = gr.variables[words[2]];
                }
                else
                {
                    newY = Int32.Parse(words[2]);
                }
            }
            catch
            {
                error.AppendText(words[2] + " is an incorrect parameter for " + words[0] + "\r\n");
            }
            if (newX != null && newY != null)
            {
                gr.xPos = (int)newX;
                gr.yPos = (int)newY;
            }
        }
    }
}
