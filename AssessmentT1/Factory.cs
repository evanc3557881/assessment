﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AssessmentT1
{
    public class Factory
    {
        TextBox error;
        Graphic gr;
        PictureBox pictureBox;
        Graphics graphics;
        public Factory(TextBox error, Graphic gr, PictureBox pictureBox, Graphics graphics)
        {
            this.error = error;
            this.gr = gr;
            this.pictureBox = pictureBox;
            this.graphics = graphics;
        }
        public command getCommand(string command)
        {
            command = command.ToLower().Trim();

            if (command.Equals("circle"))
            {
                return new Circle(error, gr, pictureBox, graphics);

            }
            else if (command.Equals("rectangle"))
            {
                return new Rectangle(error, gr, pictureBox, graphics);

            }
            else if (command.Equals("triangle"))
            {
                return new Triangle(error, gr, pictureBox, graphics);

            }
            else if (command.Equals("moveto"))
            {
                return new MoveTo(error, gr, pictureBox, graphics);

            }
            else if (command.Equals("drawto"))
            {
                return new DrawTo(error, gr, pictureBox, graphics);

            }
            else if (command.Equals("clear"))
            {
                return new Clear(error, gr, pictureBox, graphics);


            }
            else if (command.Equals("reset"))
            {
                return new Reset(error, gr, pictureBox, graphics);


            }
            else if (command.Equals("fill"))
            {
                return new Fill(error, gr, pictureBox, graphics);


            }
            else if (command.Equals("pen"))
            {
                return new PenColor(error, gr, pictureBox, graphics);
            }
            else
            {
                System.ArgumentException arg = new System.ArgumentException("Factory: " + command + " does not exist");
                throw arg;
        
            }
        }
    }
}
