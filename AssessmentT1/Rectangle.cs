﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AssessmentT1
{
    class Rectangle:command
    {
        TextBox error;
        Graphic gr;
        PictureBox pictureBox;
        Graphics graphics;
        public Rectangle(TextBox error, Graphic gr, PictureBox pictureBox, Graphics graphics)
        {
            this.error = error;
            this.gr = gr;
            this.pictureBox = pictureBox;
            this.graphics = graphics;
        }
        public void execute(string[] words)
        {
            int? width = null;//size of rectangle
            int? height = null;
            try
            {
                if (gr.checkVar(words[1]))
                {
                    width = gr.variables[words[1]];
                }
                else
                {
                    width = Int32.Parse(words[1]);
                }
            }
            catch
            {
                error.AppendText(words[1] + " is an incorrect parameter for " + words[0] + "\r\n");
            }
            try
            {
                if (gr.checkVar(words[2]))
                {
                    height = gr.variables[words[2]];
                }
                else
                {
                    height = Int32.Parse(words[2]);
                }

            }
            catch
            {
                error.AppendText(words[2] + " is an incorrect parameter for " + words[0] + "\r\n");
            }
            if (width != null && height != null)
            {
                graphics.DrawRectangle(gr.myPen, gr.xPos, gr.yPos, (int)width, (int)height);
                pictureBox.Refresh();
            }
            if (gr.fillon)/// if fill is set to true fill the rectangle
            {
                SolidBrush myBrush = new SolidBrush(gr.myPen.Color);//Used to fill the drawn shapes
                graphics.FillRectangle(myBrush, gr.xPos, gr.yPos, (int)width, (int)height);
                pictureBox.Refresh();
            }

        }
    }
}
