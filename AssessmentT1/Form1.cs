﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssessmentT1
{
    /// <summary>
    /// Creates sturcture of the form and all user interaction events are handled here
    /// </summary>
    public partial class Form1 : Form
    {
        const int formSizeX = 640;//constants for the window size of the form
        const int formSizeY = 480;

        Bitmap OutputBitmap = new Bitmap(formSizeX, formSizeY);
        Graphic myGraphic;
        Boolean flag = false;

        public Form1()
        {
            InitializeComponent();
            myGraphic = new Graphic(textBox2, Graphics.FromImage(OutputBitmap));


        }

        /// <summary>
        /// Allows for the drawing and use of pen which updates the bitmap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = e.Graphics;
            gr.DrawImageUnscaled(OutputBitmap, 0, 0);//displays the bitmap that has been drawn on onto the form

        }

        /// <summary>
        /// This is an on click event for button 1 which sends the text from richTextBox1 through the parser
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            new Parser(myGraphic, richTextBox1.Text, textBox2, pictureBox1, myGraphic.gr);

        }

        /// <summary>
        /// On click event from button two that saves the text written in richTextBox1 to a text file
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            File.WriteAllText("savedProgram.txt", richTextBox1.Text);
            richTextBox1.Clear();
        }

        /// <summary>
        /// Keydown event for the commandline that parses the text within the line when the enter key is pressed and if the text is run it 
        /// </summary>
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                textBox2.Clear();
                new Parser(myGraphic, textBox1.Text, textBox2, pictureBox1, myGraphic.gr);

                if(textBox1.Text == "run")
                {
                    new Parser(myGraphic, richTextBox1.Text, textBox2, pictureBox1, myGraphic.gr);
                }
                textBox1.Clear();

            }

        }
        /// <summary>
        /// On click event from button three that loads the text previously saved from button two into the program textbox
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            string text = File.ReadAllText("savedProgram.txt");
            richTextBox1.Text = text;
        
        }
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
        }
    }
}
    
