﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AssessmentT1
{
    class Triangle:command
    {
        TextBox error;
        Graphic gr;
        PictureBox pictureBox;
        Graphics graphics;
        public Triangle(TextBox error, Graphic gr, PictureBox pictureBox, Graphics graphics)
        {
            this.error = error;
            this.gr = gr;
            this.pictureBox = pictureBox;
            this.graphics = graphics;
        }
        public void execute(string[] words)
        {
            int? size = null;//length of each side of the triangle
            try
            {
                if (gr.checkVar(words[1]))
                {
                    size = gr.variables[words[1]];
                }
                else
                {
                    size = Int32.Parse(words[1]);

                }
                }
            catch
            {
                error.AppendText(words[1] + " is an incorrect parameter for " + words[0] + "\r\n");
            }
            Point[] points = new Point[] {new Point(gr.xPos , gr.yPos),//Created array of points which DrawPolygon will use
            new Point((int)(gr.xPos - size), (int)(gr.yPos + size)),
            new Point((int)(gr.xPos + size), (int)(gr.yPos + size)) };
            if (size != null)
            {

                graphics.DrawPolygon(gr.myPen, points);

                pictureBox.Refresh();
            }

            if (gr.fillon)/// if fill is set to true fill the triangle / polygon
            {
                SolidBrush myBrush = new SolidBrush(gr.myPen.Color);
                graphics.FillPolygon(myBrush, points);
                pictureBox.Refresh();
            }

        }
    }
}
