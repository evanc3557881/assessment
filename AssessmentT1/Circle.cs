﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AssessmentT1
{
    class Circle:command
    {
        TextBox error;
        Graphic gr;
        PictureBox pictureBox;
        Graphics graphics;
        public Circle(TextBox error, Graphic gr, PictureBox pictureBox, Graphics graphics)
        {
            this.error = error;
            this.gr = gr;
            this.pictureBox = pictureBox;
            this.graphics = graphics;
        }
        public void execute(string[] words)
        {
            int? radius = null;//radius of circle

            try
            {
                if (gr.checkVar(words[1]))
                {
                    radius = gr.variables[words[1]];
                }
                else
                {

                    radius = Int32.Parse(words[1]);
                }
            }
            catch
            {
                error.AppendText(words[1] + " is an incorrect parameter for " + words[0] + "\r\n");
            }

            if (radius != null)
            {
                graphics.DrawEllipse(gr.myPen, (int)(gr.xPos - radius / 2), (int)(gr.yPos - radius / 2), (int)radius, (int)radius);
                pictureBox.Refresh();

            }
            if (gr.fillon)/// if fill is set to true fill the circle
            {
                SolidBrush myBrush = new SolidBrush(gr.myPen.Color);
                graphics.FillEllipse(myBrush, (int)(gr.xPos - radius / 2), (int)(gr.yPos - radius / 2), (int)radius, (int)radius);
                pictureBox.Refresh();
            }
        }
    }
}
