﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AssessmentT1
{
    public class Parser
    {

        /// <summary>
        /// Parser which can be called to parse input
        /// </summary>
        /// <param name="gr"></param>
        /// <param name="input"></param>
        /// <param name="error"></param>
        /// <param name="pictureBox1"></param>
        /// <param name="graphics"></param>
        public Parser(Graphic gr, string input, TextBox error, PictureBox pictureBox1, Graphics graphics)
        {

            Factory factory = new Factory(error, gr, pictureBox1, graphics);

            bool inloop = false; 
            bool inif = false;
            bool willflash = false;
            List<string[]> loopedcommands = new List<string[]>();
            List<string[]> ifcommands = new List<string[]>();
            int loopiterations = 1;
            int ifiterations = 0;
            Color color1 = new Color();
            Color color2 = new Color();
            

            input = input.ToLower().Trim();//sets input all to lower case and removes white space
            String[] line = input.Split("\n");//splits on new line
            for (int i = 0; i < line.Length; i++) //loops round until theres no more user input  
            {
                string[] words = line[i].Split(" ");
                if(inloop)
                {
                    if (words[0] == "endfor")
                    {
                        inloop = false;
                        Executeloop(loopedcommands, ref loopiterations, words, gr, error, factory, ref inloop, ifcommands, ref ifiterations, ref inif, ref willflash, color1, color2);
                        loopedcommands.Clear();
                    }
                    else
                    {
                        loopedcommands.Add(words);
                    }
                  
                }
                else if (inif)
                {
                    if (words[0] == "endif")
                    {
                        inif = false;
                        Executeloop(ifcommands, ref ifiterations, words, gr, error, factory, ref inloop, ifcommands, ref ifiterations, ref inif, ref willflash, color1, color2);
                        ifcommands.Clear();
                    }
                    else
                    {
                        ifcommands.Add(words);
                    }

                }
                else
                {
                 Select(words, gr, error, factory, ref inloop, ref loopiterations, loopedcommands, ifcommands, ref ifiterations, ref inif, ref willflash, color1, color2);
                }
            }
        }

        /// <summary>
        /// function that executes all the commands within a loop used for if and for loops
        /// </summary>
        /// <param name="loopedcommands"></param>
        /// <param name="loopiterations"></param>
        /// <param name="words"></param>
        /// <param name="gr"></param>
        /// <param name="error"></param>
        /// <param name="factory"></param>
        /// <param name="inloop"></param>
        /// <param name="ifcommands"></param>
        /// <param name="ifiterations"></param>
        /// <param name="inif"></param>
        public void Executeloop(List<string[]> loopedcommands, ref int loopiterations, string[] words, Graphic gr, TextBox error, Factory factory, ref bool inloop, List<string[]> ifcommands, ref int ifiterations, ref bool inif, ref bool willflash, Color color1, Color color2)
        {
            for (int i = 0; i < loopiterations; i++)
            {
                for(int j  = 0; j < loopedcommands.Count; j++)
                {
                    Select(loopedcommands[j], gr, error, factory, ref inloop, ref loopiterations, loopedcommands, ifcommands, ref ifiterations, ref inif, ref willflash, color1, color2);
                } 
            }
        }

        /// <summary>
        /// used to check what operation is used within the if statment
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="arguement1"></param>
        /// <param name="arguement2"></param>
        /// <returns></returns>
        public bool Performcomparison(string operation, int arguement1, int arguement2)
        {
            if(operation == "<")
            {
                return arguement1 < arguement2;
            }
            else if(operation == ">")
            {
                return arguement1 > arguement2;
            }
            else if(operation == "<=")
            {
                return arguement1 <= arguement2;
            }
            else if(operation == ">=")
            {
                return arguement1 >= arguement2;
            }
            else if(operation == "!=")
            {
                return arguement1 != arguement2;
            }
            else if (operation == "==")
            {
                return arguement1 == arguement2;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="words"></param>
        /// <param name="gr"></param>
        /// <param name="error"></param>
        /// <param name="factory"></param>
        /// <param name="inloop"></param>
        /// <param name="loopiterations"></param>
        /// <param name="loopedcommands"></param>
        /// <param name="ifcommands"></param>
        /// <param name="ifiterations"></param>
        /// <param name="inif"></param>
        public void Select(string[] words, Graphic gr, TextBox error, Factory factory, ref bool inloop, ref int loopiterations, List<string[]> loopedcommands, List<string[]> ifcommands, ref int ifiterations, ref bool inif, ref bool willflash, Color color1, Color color2)
        {
            command moveTo = factory.getCommand("moveto");
            command drawTo = factory.getCommand("drawto");
            command circle = factory.getCommand("circle");
            command triangle = factory.getCommand("triangle");
            command rectangle = factory.getCommand("rectangle");
            command clear = factory.getCommand("clear");
            command reset = factory.getCommand("reset");
            command pen = factory.getCommand("pen");
            command fill = factory.getCommand("fill");
          /*  if (!willflash)
            {*/
                if (words[0] == "drawto")
                {
                    if (words.Length == 3)//if there are not 3 parameters returns incorrect amount of parameters (drawto, X, Y)
                    {
                        drawTo.execute(words);
                    }
                    else
                    {
                        error.AppendText(words[0] + " needs two parameters\r\n");
                    }

                }
                else if (words[0] == "moveto")
                {
                    if (words.Length == 3)
                    {
                        moveTo.execute(words);
                    }
                    else
                    {
                        error.AppendText(words[0] + " needs two parameters\r\n");
                    }
                }
                else if (words[0] == "rect")
                {
                    if (words.Length == 3)
                    {
                        rectangle.execute(words);
                    }
                    else
                    {
                        error.AppendText(words[0] + " needs two parameters\r\n");
                    }

                }
                else if (words[0] == "circle")
                {
                    if (words.Length == 2)
                    {
                        circle.execute(words);
                    }
                    else
                    {
                        error.AppendText(words[0] + " needs one parameters\r\n");
                    }

                }
                else if (words[0] == "triangle")
                {
                    if (words.Length == 2)
                    {
                        triangle.execute(words);
                    }
                    else
                    {
                        error.AppendText(words[0] + " needs one parameters\r\n");
                    }

                }
                else if (words[0] == "clear")
                {
                    clear.execute(words);
                }
                else if (words[0] == "reset")
                {
                    reset.execute(words);
                }
                else if (words[0] == "pen")
                {
                    if (words.Length == 2)
                    {
                        pen.execute(words);
                    }
                    else
                    {
                        error.AppendText(words[0] + " needs one parameters\r\n");
                    }

                }
                else if (words[0] == "fill")
                {
                    fill.execute(words);
                }
                else if (words[0] == "run")
                {
                }
                else if (words[0] == "var")
                {
                    if (words.Length == 3)
                    {
                        gr.Var(words, error);
                    }
                    else if (words.Length == 5)
                    {
                        if ("+/-*".Contains(words[3]))
                        {
                            gr.Var(words, error);
                        }
                        else
                        {
                            error.AppendText(words[3] + " needs to be +,/,* or - \r\n");
                        }
                    }
                    else
                    {
                        error.AppendText(words[0] + " needs two or four parameters \r\n");
                    }

                }

                else if (words[0] == "for")
                {
                    inloop = true;
                    try
                    {
                        if (gr.checkVar(words[1]))
                        {
                            loopiterations = gr.variables[words[1]];
                        }
                        else
                        {
                            loopiterations = Int32.Parse(words[1]);
                        }
                    }
                    catch
                    {
                        error.AppendText(words[1] + " is not and integer\r\n");
                        loopiterations = 1;
                    }

                }
                else if (words[0] == "endfor")
                {
                    if (!inloop)
                    {
                        error.AppendText(words[0] + ": You're not in a loop\r\n");
                    }
                }
                else if (words[0] == "if")
                {
                    int arguement1 = 0;
                    int arguement2 = 0;
                    string operation = words[2];
                    if (operation == "<" || operation == ">" || operation == ">=" || operation == "<=" || operation == "!=" || operation == "==")
                    {
                        try
                        {
                            if (gr.checkVar(words[1]))
                            {
                                arguement1 = gr.variables[words[1]];
                            }
                            else
                            {
                                arguement1 = Int32.Parse(words[1]);
                            }
                        }
                        catch
                        {
                            error.AppendText(words[1] + " needs to be and int or a variable \r\n");
                        }
                        try
                        {
                            if (gr.checkVar(words[3]))
                            {
                                arguement2 = gr.variables[words[3]];
                            }
                            else
                            {
                                arguement2 = Int32.Parse(words[3]);
                            }
                        }
                        catch
                        {
                            error.AppendText(words[3] + " needs to be and int or a variable \r\n");
                        }
                        inif = true;
                        if (Performcomparison(operation, arguement1, arguement2))
                        {
                            ifiterations = 1;
                        }
                        else
                        {
                            ifiterations = 0;
                        }
                    }
                    else
                    {
                        error.AppendText(operation + " needs to be <, >, >=, <=, != or == \r\n");
                    }

                }
                else if (words[0] == "endif")
                {
                    if (!inif)
                    {
                        error.AppendText(words[0] + ": You're not in an if statment \r\n");
                    }
                }
/*                else if (words[0] == "flash")
                {
                    if (words.Length == 3)
                    {

                        try
                        {
                            willflash = true;
                            color1 = Color.FromName(words[1]);
                            color2 = Color.FromName(words[2]);

                        }
                        catch
                        {
                            error.AppendText(words[1] + words[2] + ": These are not colors \r\n");
                        }
                    }
                    else
                    {
                        error.AppendText(words[0] + ": needs to be two arguements \r\n");
                    }
                }*/
                else if (words[0] != "")
                {
                    error.AppendText(words[0] + " is an incorrect command\r\n");
                }
            }
/*            else
            {
                command commands = null;
                if (words[0] == "drawto")
                {
                    commands = drawTo;
                }
                else if (words[0] == "circle")
                {
                    commands = circle;
                }
                else if (words[0] == "triangle")
                {
                    commands = triangle;
                }
                else if (words[0] == "rect")
                {
                    commands = rectangle;
                }
                else
                {
                    error.AppendText(words[0] + " needs to be a valid drawing command \r\n");
                }
                gr.flashing.execute(words, commands, color1, color2);
                willflash = false;
            }*/
        }



    }

}



