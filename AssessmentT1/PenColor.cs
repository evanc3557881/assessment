﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AssessmentT1
{
    class PenColor:command
    {
        TextBox error;
        Graphic gr;
        PictureBox pictureBox;
        Graphics graphics;
        public PenColor(TextBox error, Graphic gr, PictureBox pictureBox, Graphics graphics)
        {
            this.error = error;
            this.gr = gr;
            this.pictureBox = pictureBox;
            this.graphics = graphics;
        }
        public void execute(string[] words)
        {
            string color = words[1];

            gr.myPen = new Pen(Color.FromName(color));//sets the pen to the users input
            pictureBox.Refresh();
        }
    }
}
