using Microsoft.VisualStudio.TestTools.UnitTesting;
using AssessmentT1;
using System.Drawing;

namespace AssessmentTests
{
    /// <summary>
    /// Unit tests for the assessment
    /// </summary>
    [TestClass]
    public class AssessmentTests
    {
        /// <summary>
        /// Test move to command that it is accurate and uses correct parameters
        /// </summary>
        [TestMethod]
        public void TestingMovetoCommand()
        {


            Bitmap OutputBitmap = new Bitmap(640, 480);
            Graphic test1 = new Graphic(Graphics.FromImage(OutputBitmap));

            test1.moveTo(new string[] { "moveTo", "100", "150"} , new System.Windows.Forms.TextBox());//gives parameters to test

            Assert.AreEqual(100, test1.xPos, 0.1);
            Assert.AreEqual(150, test1.yPos, 0.1);

        }

        /// <summary>
        /// Test the reset command reset the pointer to the correct position
        /// </summary>
        [TestMethod]
        public void TestingReset()
        {
            Bitmap OutputBitmap = new Bitmap(640, 480);
            Graphic test2 = new Graphic(Graphics.FromImage(OutputBitmap));

            test2.reset(new System.Windows.Forms.PictureBox());

            Assert.AreEqual(0, test2.xPos, 0.1);
            Assert.AreEqual(0, test2.yPos, 0.1);

        }
           
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Part2 TESTS

      
        /// <summary>
        /// Test the Variable command that has not yet been implemented (part 2 will return failed)
        /// </summary>
        [TestMethod]
        public void TestingVar()
        {

            Bitmap OutputBitmap = new Bitmap(640, 480);
            Graphic test4 = new Graphic(Graphics.FromImage(OutputBitmap));

            test4.Var(new string[] { "var", "100"}, new System.Windows.Forms.TextBox());
            Assert.AreEqual(100, test4.variables[test4.variables.Count-1]);


        }

    }
}
